# 赛博朋克
赛博朋克风格是一个非常独特的游戏美术风格，通常描绘未来世界的黑暗和充满危险的环境。它是一个结合了科技和社会问题的风格，着重于对社会、文化等问题的思考，以及高科技元素的创新与使用。
赛博朋克风格通常被描述为“高科技低生活”，也就是说，未来科技虽然高度发达，但社会、人类关系和生活水平却严重恶化。在赛博朋克游戏中，玩家通常扮演着“反抗者”或“猎头”等不同的角色，与政府或大企业作斗争，探索着这个黑暗而危险的未来世界。